'use strict';

angular.module('pakiitoApp')
  .controller('AdminCtrl', function ($scope,$rootScope, $http, Auth, User,socket,$modal,$log) {

    // Use the User $resource to fetch all users
    $scope.users = User.query();
    $scope.transactions = [];

    $scope.delete = function(user) {
      User.remove({ id: user._id });
      angular.forEach($scope.users, function(u, i) {
        if (u === user) {
          $scope.users.splice(i, 1);
        }
      });
    };
    
     $http.get('/api/transactions').success(function(transaction) {
      $scope.transactions = transaction;
      socket.syncUpdates('transaction', $scope.transactions);
    });
    
      $scope.editstatus = function (tranid,toid) {
          $rootScope.transactionid = tranid; 
          $rootScope.toid = toid;
       var modalInstance = $modal.open({
      animation: $scope.animationsEnabled,
      templateUrl:'app/editstatus/editstatus.html',
      controller: 'ModalInstanceCtrl2', // instance
      size: 'sm',
      resolve: {
        
      }
    });
    
    modalInstance.result.then(function () {
     // $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
   
      };
    
     $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  };
    
  }).controller('ModalInstanceCtrl2', function ($scope,$http,$rootScope,$modalInstance) {

  $scope.ok = function () {
      console.log($scope.tranStatus);   
      console.log($rootScope.transactionid);
      console.log();
      
          
          
           $http.post('/api/transactions/updateStatus', { 
           Status: $scope.tranStatus,
            trackid:$rootScope.transactionid,
            id:$rootScope.toid
         });
          
          $modalInstance.close();
          
          };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  });

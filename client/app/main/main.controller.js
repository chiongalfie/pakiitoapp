'use strict';

angular.module('pakiitoApp')
  .controller('MainCtrl', function ($scope, $http, socket,$rootScope,$modal) {
    $scope.awesomeThings = [];
	$scope.Services = [];
    $http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
      socket.syncUpdates('thing', $scope.awesomeThings);
    });

	  $http.get('/api/services').success(function(services) {
      $scope.Services = services;
      socket.syncUpdates('service', $scope.Services);
    });
    $scope.addThing = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/things', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/things/' + thing._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('thing');
    });
	
	$scope.animationsEnabled = true;
var modalInstance;
  $scope.open = function (me) {
$rootScope.serviceName = me;

    modalInstance = $modal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'app/requestService/requestService.html',
      controller: 'ModalInstanceCtrl3', // instance
      size: 'lg',
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  };
  })
  //ModalInstanceCtrl IS USE by modal to instantiate scope on open line51
          .controller('ModalInstanceCtrl3', function ($scope,$http,$rootScope, $modalInstance, items) {

  $scope.items = items;
  
  $scope.selected = {
  //  item: $scope.items[0]
  };

  $scope.ok = function () {
      
      console.log($scope.clientName);

		console.log($scope.clientName.$valid);
	
     /*if($scope.fullname === '' ||
          $scope.email === '' ||
          $scope.contactNumber === ''
                  ) {
        return;
      }*/
      $http.post('/api/transactions', { 
            name: $scope.clientName,
            serviceName:$rootScope.serviceName,
            email:$scope.email,
            contactNo:$scope.contactNumber,
            note:$scope.notes,
            address: $scope.location,
            Status: "Verifying",
            trackid:$scope.clientName.replace(/\s/g, '')+Math.random().toString(36).substring(7)+(new Date().getTime())
         });
      $scope.clientName = '';
      $scope.email = '';
      $scope.contactNumber = '';
      $scope.notes = '';
      $scope.location = '';  
      
		  
    $modalInstance.close($scope.selected.item);
    
      };
	  
$scope.createRequest =function(form){
	//$scope.submitted = true; 	
	$scope.submitted = true; 			
	validate(form.$valid);
		  
  
    
	//}
};


function validate(isValid){
  
 // console.log($scope.imgname);
 // console.log($scope.userId);
	if(isValid) {
        var cname = $scope.clientName.replace(" ", "");
		$http.post('/api/transactions', { 
            name: $scope.clientName,
            serviceName:$rootScope.serviceName,
            email:$scope.email,
            contactNo:$scope.contactNumber,
            note:$scope.notes,
            address: $scope.location,
            Status: "Verifying",
            trackid:cname+Math.random().toString(36).substring(7)+(new Date().getTime())
         })
        .then( function(respon) {
		  //console.log(respon.id);

		//Auth.logout();
        //$location.path('/');
		//Modal.open();

	/*	$modal.open({
      templateUrl: 'modal1.html',
      controller: 'ContestCtrl',
      size: "lg",
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });*/
		
		$scope.clientName = '';
      $scope.email = '';
      $scope.contactNumber = '';
      $scope.notes = '';
      $scope.location = ''; 

  $modalInstance.close($scope.selected.item);	  
	 	})
        .catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
	  
	 
	};

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  });

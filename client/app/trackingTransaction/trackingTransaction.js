'use strict';

angular.module('pakiitoApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/trackingTransaction', {
        templateUrl: 'app/trackingTransaction/trackingTransaction.html',
        controller: 'TrackingTransactionCtrl'
      });
  });

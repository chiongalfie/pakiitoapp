'use strict';

describe('Controller: TrackingTransactionCtrl', function () {

  // load the controller's module
  beforeEach(module('pakiitoApp'));

  var TrackingTransactionCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TrackingTransactionCtrl = $controller('TrackingTransactionCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});

'use strict';

angular.module('pakiitoApp')
  .directive('requestService', function () {
    return {
      templateUrl: 'app/requestService/requestService.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
  
  // Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

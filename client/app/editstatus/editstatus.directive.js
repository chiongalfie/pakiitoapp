'use strict';

angular.module('pakiitoApp')
  .directive('editstatus', function () {
    return {
      templateUrl: 'app/editstatus/editstatus.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
'use strict';

describe('Directive: editstatus', function () {

  // load the directive's module and view
  beforeEach(module('pakiitoApp'));
  beforeEach(module('app/editstatus/editstatus.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<editstatus></editstatus>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the editstatus directive');
  }));
});
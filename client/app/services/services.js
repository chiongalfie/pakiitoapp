'use strict';

angular.module('pakiitoApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/services', {
        templateUrl: 'app/services/services.html',
        controller: 'ServicesCtrl'
      });
  });

'use strict';

angular.module('pakiitoApp')
  .controller('ServicesCtrl', function ($scope,$rootScope,$http,socket,Auth,$modal,$log,Upload) {
    $scope.message = 'Hello';
    
        $scope.Services = [];
        
        $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $http.get('/api/services').success(function(services) {
      $scope.Services = services;
      socket.syncUpdates('service', $scope.Services);
    });
	
  
	$scope.imageme = "";
	

$scope.uploadFile = function(files) {
    var fd = new FormData();
    //Take the first selected file
    fd.append("file", files[0]);
	
	///console.log(files[0]);
	
	 var file = files[0];
      $scope.upload = Upload.upload({
        url: 'api/services/upload', // upload.php script, node.js route, or servlet url
        method: 'POST' ,
        //headers: {'Authorization': 'xxx'}, // only for html5
        //withCredentials: true,
        data: {myObj: $scope.myModelObj},
        file: file, // single file or a list of files. list is only for html5
        //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
        //fileFormDataName: myFile, // file formData name ('Content-Disposition'), server side request form name
                                    // could be a list of names for multiple files (html5). Default is 'file'
        //formDataAppender: function(formData, key, val){}  // customize how data is added to the formData. 
                                                        // See #40#issuecomment-28612000 for sample code

      }).progress(function(evt) {
        console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name );
		//console.log(JSON.stringify(evt))
	  }).success(function(data, status, headers, config) {
        // file is uploaded successfully
       // console.log('file ' + config.file.name + 'is uploaded successfully. Response: ' + data);
	  console.log(JSON.stringify(data[1].newpath));
	  $scope.uploadedImg = data[1].newpath;
	  }).error(function (reply, status, headers) {
		console.log(reply)
});
}


	
    //adding new services
     $scope.addServices = function(form) {
	$scope.submitted = true; 		
	form.$valid = true;
	validateServ(form.$valid);
	console.log(JSON.stringify($scope.imageme));

    };
	
	function validateServ(isValid){
 
 // console.log($scope.imgname);
 
	if(isValid) {
       $http.post('/api/services', { 
          name: $scope.newServices,
          rateDaily:$scope.rateDaily,
          jd:$scope.jd,
		  imagepath:$scope.uploadedImg
         })
        .then( function(respon) {
		  //console.log(respon.id);
$scope.newServices = '';
      $scope.jd = '';
      $scope.rateDaily = '';
 
	 	})
        .catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
			console.log(error.message);
          });
        });
      }
	  
	 
	};
    
      $scope.deleteService = function(service) {
      $http.delete('/api/services/' + service._id);
    };
    
     $scope.$on('$destroy', function () {
      socket.unsyncUpdates('service');
    });
    
  
  $scope.items = ['item1', 'item2', 'item3'];
  
  $scope.animationsEnabled = true;
var modalInstance;
  $scope.open = function (me) {
$rootScope.serviceName = me;

    modalInstance = $modal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'app/requestService/requestService.html',
      controller: 'ModalInstanceCtrl', // instance
      size: 'lg',
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  };
  
  
    //adding new services
     $scope.addTransaction = function() {
         
     
    };
  
  })
    //ModalInstanceCtrl IS USE by modal to instantiate scope on open line51
          .controller('ModalInstanceCtrl', function ($scope,$http,$rootScope, $modalInstance, items) {

  $scope.items = items;
  
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
      
      console.log($scope.clientName);

		console.log($scope.clientName.$valid);
	
     /*if($scope.fullname === '' ||
          $scope.email === '' ||
          $scope.contactNumber === ''
                  ) {
        return;
      }*/
      $http.post('/api/transactions', { 
            name: $scope.clientName,
            serviceName:$rootScope.serviceName,
            email:$scope.email,
            contactNo:$scope.contactNumber,
            note:$scope.notes,
            address: $scope.location,
            Status: "Verifying",
            trackid:$scope.clientName.replace(/\s/g, '')+Math.random().toString(36).substring(7)+(new Date().getTime())
         });
      $scope.clientName = '';
      $scope.email = '';
      $scope.contactNumber = '';
      $scope.notes = '';
      $scope.location = '';  
      
		  
    $modalInstance.close($scope.selected.item);
    
      };
	  
$scope.createRequest =function(form){
	//$scope.submitted = true; 	
	$scope.submitted = true; 			
	validate(form.$valid);
		  
  
    
	//}
};


function validate(isValid){
  
 // console.log($scope.imgname);
 // console.log($scope.userId);
	if(isValid) {
        var cname = $scope.clientName.replace(" ", "");
		$http.post('/api/transactions', { 
            name: $scope.clientName,
            serviceName:$rootScope.serviceName,
            email:$scope.email,
            contactNo:$scope.contactNumber,
            note:$scope.notes,
            address: $scope.location,
            Status: "Verifying",
            trackid:cname+Math.random().toString(36).substring(7)+(new Date().getTime())
         })
        .then( function(respon) {
		  //console.log(respon.id);

		//Auth.logout();
        //$location.path('/');
		//Modal.open();

	/*	$modal.open({
      templateUrl: 'modal1.html',
      controller: 'ContestCtrl',
      size: "lg",
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });*/
		
		$scope.clientName = '';
      $scope.email = '';
      $scope.contactNumber = '';
      $scope.notes = '';
      $scope.location = ''; 

  $modalInstance.close($scope.selected.item);	  
	 	})
        .catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
	  
	 
	};

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  
  
});

  
  

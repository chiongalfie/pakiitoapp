'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    //uri:'mongodb://alfie:wala123@ds038888.mongolab.com:38888/pakiitodb'
uri:'mongodb://localhost/pakiito-dev'
  },

  seedDB: true
};

'use strict';

var express = require('express');
var controller = require('./transaction.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/status/:id', controller.showStatus);
router.post('/', controller.create);
router.post('/updateStatus', controller.updateStatus);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
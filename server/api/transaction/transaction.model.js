'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TransactionSchema = new Schema({
  name: String,
  email:String,
  contactNo:String,
  note:String,
  address: String,
  Status: String,
  trackid:String,
  serviceName:String
  
});

module.exports = mongoose.model('Transaction', TransactionSchema);
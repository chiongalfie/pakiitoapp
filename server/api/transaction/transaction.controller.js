'use strict';

var _ = require('lodash');
var nodemailer = require("nodemailer");
var Transaction = require('./transaction.model');

// Get list of transactions
exports.index = function(req, res) {
  Transaction.find(function (err, transactions) {
    if(err) { return handleError(res, err); }
    return res.json(200, transactions);
  });
};

// Get a single transaction
exports.show = function(req, res) {
  Transaction.findById(req.params.id, function (err, transaction) {
    if(err) { return handleError(res, err); }
    if(!transaction) { return res.send(404); }
    return res.json(transaction);
  });
};

// Get a single transaction
exports.showStatus = function(req, res) {
  Transaction.find({"trackid":req.params.id}, function (err, transaction) {
    if(err) { return handleError(res, err); }
    if(!transaction) { return res.send(404); }
    return res.json(transaction);
  });
};

// Creates a new transaction in the DB.
exports.create = function(req, res) {
  Transaction.create(req.body, function(err, transaction) {
    if(err) { return handleError(res, err); }
	console.log(transaction.trackid);
	res.setHeader('content-type', 'text/html');
	autorespondTransaction(transaction.email,transaction.trackid,res);
    return res.json(201, transaction);
  });
};

// Creates a new transaction in the DB.
exports.updateStatus = function(req, res) {
 if(req.body.id) { delete req.body._id; }
  Transaction.findById(req.body.id, function (err, transaction) {
    if (err) { return handleError(res, err); }
    if(!transaction) { return res.send(404); }
    var updated = _.merge(transaction, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, transaction);
    });
  });

};

// Updates an existing transaction in the DB.
exports.update = function(req, res) {
  if(req.body.id) { delete req.body._id; }
  Transaction.findById(req.params.id, function (err, transaction) {
    if (err) { return handleError(res, err); }
    if(!transaction) { return res.send(404); }
    var updated = _.merge(transaction, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, transaction);
    });
  });
};

// Deletes a transaction from the DB.
exports.destroy = function(req, res) {
  Transaction.findById(req.params.id, function (err, transaction) {
    if(err) { return handleError(res, err); }
    if(!transaction) { return res.send(404); }
    transaction.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function autorespondTransaction(clientemail,transid,res){
	console.log(clientemail);
	
	var smtpTransport = nodemailer.createTransport("SMTP",{
service: "Gmail",
auth: {
user: "chiong.alfie@gmail.com",
pass: "walalang1"
}
});

var mailOptions={
to : clientemail,
subject : "No reply this is auto generated: ",
html : "email: "+clientemail+" <br> Thank you for requesting a service <br>  pls wait for our customer service personel to contact you ton confirm your transaction <br>here is your transaction id "+transid+
     " you may check your current transaction status here : http://localhost:9000/trackingTransaction"
//text: send as text
}
console.log(mailOptions);


  //  return res.json(mailOptions);
  
 smtpTransport.sendMail(mailOptions, function(error, response){
if(error){
console.log(error);

//return res.json(mailOptions);
}else{
console.log("Message sent: " + response.message);
//return res.json(mailOptions);
res.end();
}
});  
}

function handleError(res, err) {
  return res.send(500, err);
}
'use strict';

var express = require('express');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var controller = require('./service.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/',multipartyMiddleware, controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.post('/upload',multipartyMiddleware,controller.uploadFile);
module.exports = router;
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ServiceSchema = new Schema({
  name: String,
  info: String,
  active: Boolean,
  rateDaily:Number,
  jd:String,
  imagepath:String
  });

module.exports = mongoose.model('Service', ServiceSchema);
'use strict';

var _ = require('lodash');
var formidable = require('formidable'),
util = require('util'),
fs   = require('fs-extra');
var path = require('path');
var Service = require('./service.model');

// Get list of services
exports.index = function(req, res) {
  Service.find(function (err, services) {
    if(err) { return handleError(res, err); }
    return res.json(200, services);
  });
};

// Get a single service
exports.show = function(req, res) {
  Service.findById(req.params.id, function (err, service) {
    if(err) { return handleError(res, err); }
    if(!service) { return res.send(404); }
    return res.json(service);
  });
};

// Creates a new service in the DB.
exports.create = function(req, res) {
 Service.create(req.body, function(err, service) {
    if(err) { return handleError(res, err); }
   return res.json(201, service);
  });
 
};

// Updates an existing service in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Service.findById(req.params.id, function (err, service) {
    if (err) { return handleError(res, err); }
    if(!service) { return res.send(404); }
    var updated = _.merge(service, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, service);
    });
  });
};

// Deletes a service from the DB.
exports.destroy = function(req, res) {
  Service.findById(req.params.id, function (err, service) {
    if(err) { return handleError(res, err); }
    if(!service) { return res.send(404); }
    service.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function uploadme(req,res){
	var newpath1 = "";
    var file = req.files.file;
	var uniqueNumber = new Date().getTime();
	var file_name = file.name;
	//console.log(file.path);
    console.log(file.name);
    console.log(file.type);
	console.log(file.size);
	var new_location = 'client/assets/images/uploads/';
	var temp_path = file.path;
	var newpath = new_location+uniqueNumber+ file_name;
	fs.copy(temp_path, newpath, function(err) {  
      if (err) {
        console.error(err);
      } else {
       console.log("success!" + newpath)
		//var newpath1 = newpath;
		//return res.json({"newpath":uniqueNumber+ file_name});
		return res.json({"newpath":uniqueNumber+ file_name});
     }
	  }); 
}

exports.uploadFile = function(req,res){
// We are able to access req.files.file thanks to 
    // the multiparty middleware
	var newpath1 = "";
    var file = req.files.file;
	var uniqueNumber = new Date().getTime();
	var file_name = file.name;
	//console.log(file.path);
    console.log(file.name);
    console.log(file.type);
	console.log(file.size);
	var new_location = 'public/assets/images/uploads/';
	var temp_path = file.path;
	var newpath = new_location+uniqueNumber+ file_name;
	fs.copy(temp_path, newpath, function(err) {  
      if (err) {
        console.error(err);
      } else {
       console.log("success!" + newpath)
		//var newpath1 = newpath;
		return res.json([{"body":req.body},{"newpath":uniqueNumber+ file_name}]);
     }
	  }); 
 // return res.json([{"body":req.body},{"newpath":newpath1}]);
}

function handleError(res, err) {
  return res.send(500, err);
}
'use strict';

var _ = require('lodash');
var nodemailer = require("nodemailer");
var Sendmail = require('./sendmail.model');

// Get list of sendmails
exports.index = function(req, res) {
  Sendmail.find(function (err, sendmails) {
    if(err) { return handleError(res, err); }
    return res.json(200, sendmails);
  });
};

// Get a single sendmail
exports.show = function(req, res) {
  Sendmail.findById(req.params.id, function (err, sendmail) {
    if(err) { return handleError(res, err); }
    if(!sendmail) { return res.send(404); }
    return res.json(sendmail);
  });
};

// Creates a new sendmail in the DB.
exports.create = function(req, res) {
  Sendmail.create(req.body, function(err, sendmail) {
    if(err) { return handleError(res, err); }
    return res.json(201, sendmail);
  });
};

// Updates an existing sendmail in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Sendmail.findById(req.params.id, function (err, sendmail) {
    if (err) { return handleError(res, err); }
    if(!sendmail) { return res.send(404); }
    var updated = _.merge(sendmail, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, sendmail);
    });
  });
};

// Deletes a sendmail from the DB.
exports.destroy = function(req, res) {
  Sendmail.findById(req.params.id, function (err, sendmail) {
    if(err) { return handleError(res, err); }
    if(!sendmail) { return res.send(404); }
    sendmail.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};




// Creates a new sendmail in the DB.
exports.sendtransaction = function(req, res) {
	console.log(req.body.email);
	res.setHeader('content-type', 'text/html');



	var smtpTransport = nodemailer.createTransport("SMTP",{
service: "Gmail",
auth: {
user: "chiong.alfie@gmail.com",
pass: "walalang1"
}
});

var mailOptions={
to : "alfiechiong@gmail.com",
subject : "Inquiry from: "+req.body.subject,
html : "email: "+req.body.email+" <br>"+ req.body.message
//text: send as text
}
console.log(mailOptions);


  //  return res.json(mailOptions);
  
 smtpTransport.sendMail(mailOptions, function(error, response){
if(error){
console.log(error);

//return res.json(mailOptions);
}else{
console.log("Message sent: " + response.message);
//return res.json(mailOptions);
res.end();

}
});  
	
return res.json(200, {email:"sent"});
};



function handleError(res, err) {
  return res.send(500, err);
}